class AuthenticateAdmin
  prepend SimpleCommand

  def initialize(username, password)
    @username = username
    @password = password
  end

  def call
    encode(admin_id: admin.id) if admin
  end

  attr_accessor :username, :password



  def admin
    admin = Admin.find_by_username(username)
    return admin if admin && admin.authenticate(password)
    errors.add :admin_authentication, 'invalid credentials'
    nil
  end

  def encode(payload, exp = 6.month.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new body
  rescue
    nil
  end

end
