class AuthorizeApiRequestAdmin
  prepend SimpleCommand

  def decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new body
  rescue
    nil
  end

  def encode(payload, exp = 1200.hours.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def initialize(headers = {})
    @headers = headers
  end

  def call
    admin
  end


  attr_reader :headers


  def admin
    @admin ||= Admin.find(decoded_auth_token[:admin_id]) if decoded_auth_token
    @admin || errors.add(:token, 'Invalid token') && nil
  end

  def decoded_auth_token
    decoded_auth_token ||= decode(http_auth_header)
  end

  def http_auth_header
    if headers['Authorization'].present?
      return headers['Authorization'].split(' ').last
    else
      errors.add(:token, 'Missing token')
    end
    nil
  end
end
