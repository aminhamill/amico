class ApplicationController < ActionController::Base
  before_action :set_headers

  def set_headers
   puts 'ApplicationController.set_headers'
   headers['Access-Control-Allow-Origin'] = '*'
   headers['Access-Control-Expose-Headers'] = 'ETag'
   headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
   headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match,Auth-User-Token'
   headers['Access-Control-Max-Age'] = '86400'
   headers['Access-Control-Allow-Credentials'] = 'true'
  end


  def authenticate_request_user
    @current_user = AuthorizeApiRequestUser.call(request.headers).result
    render json: { error: 'Not Authorized. please signin' }, status: 401 unless @current_user
  end

  def authenticate_request_admin
    @current_user = AuthorizeApiRequestAdmin.call(request.headers).result
    render json: { error: 'Not Authorized. please signin' }, status: 401 unless @current_admin
  end

end
