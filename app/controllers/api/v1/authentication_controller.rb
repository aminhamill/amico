class Api::V1::AuthenticationController < ApplicationController

  def authenticate_user
    user = User.where(username: params[:username]).first
    unless user
      render json: { message: "user not found." }, status: 404
      return
    end
    command = AuthenticateUser.call(params[:username], params[:password])
    user = User.find_by(username: params[:username])
    if command.success?
      unless user.verificated_at
        render json: { message: "your account not activated." }, status: 402
        return
      end
      if user.active?
        render json: { token: command.result }
      else
        render json: { message: "your account is deactive." }, status: 403
      end
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def authenticate_admin
    command = AuthenticateAdmin.call(params[:username], params[:password])
    if command.success?
      render json: { token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

end
