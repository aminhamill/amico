class User < ApplicationRecord
  has_many :user_devices
  has_many :devices, through: :user_devices
  has_many :exams

  enum status: [:active,:deactive]
end
