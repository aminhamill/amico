class Ticket < ApplicationRecord
  belongs_to :user
  belongs_to :admin
  belongs_to :user_device
end
