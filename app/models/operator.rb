class Operator < ApplicationRecord
  belongs_to :user_device
  has_many :exams
end
