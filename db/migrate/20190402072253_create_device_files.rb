class CreateDeviceFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :device_files do |t|
      t.references :device, foreign_key: true
      t.string :name
      t.integer :category
      t.integer :file_type
      t.string :file

      t.timestamps
    end
  end
end
