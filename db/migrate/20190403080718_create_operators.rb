class CreateOperators < ActiveRecord::Migration[5.2]
  def change
    create_table :operators do |t|
      t.string :first_name
      t.string :last_name
      t.references :user_device, foreign_key: true
      t.integer :operator_type
      t.datetime :started_at
      t.string :phone_number
      t.integer :exam_status

      t.timestamps
    end
  end
end
