class CreateExams < ActiveRecord::Migration[5.2]
  def change
    create_table :exams do |t|
      t.references :operator, foreign_key: true
      t.datetime :date
      t.string :exam_time
      t.integer :score
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
