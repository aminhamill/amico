class CreateUserDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :user_devices do |t|
      t.references :device, foreign_key: true
      t.references :user, foreign_key: true
      t.datetime :purchase_date
      t.datetime :guarantee_started_at
      t.integer :guarantee_duration

      t.timestamps
    end
  end
end
