class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices do |t|
      t.string :name
      t.integer :category
      t.string :series
      t.integer :country
      t.string :image1
      t.string :image2
      t.string :image3
      t.string :image4
      t.text :description
      t.integer :status

      t.timestamps
    end
  end
end
