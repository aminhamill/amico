class CreateLookups < ActiveRecord::Migration[5.2]
  def change
    create_table :lookups do |t|
      t.string :title
      t.integer :value
      t.string :category

      t.timestamps
    end
  end
end
