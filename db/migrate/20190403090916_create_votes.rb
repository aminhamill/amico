class CreateVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :votes do |t|
      t.integer :category
      t.string :question
      t.integer :score_type

      t.timestamps
    end
  end
end
