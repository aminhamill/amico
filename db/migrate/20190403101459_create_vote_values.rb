class CreateVoteValues < ActiveRecord::Migration[5.2]
  def change
    create_table :vote_values do |t|
      t.references :vote, foreign_key: true
      t.references :user, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
