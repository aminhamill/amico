class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :company_name
      t.string :username
      t.string :password
      t.string :phone_number
      t.string :logo
      t.string :signature
      t.integer :status
      t.text :description

      t.timestamps
    end
  end
end
