class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.references :user, foreign_key: true
      t.references :admin, foreign_key: true
      t.references :user_device, foreign_key: true
      t.string :certificate
      t.integer :category
      t.string :subject
      t.integer :status
      t.text :body
      t.integer :replay_to
      t.integer :priority
      t.integer :ward
      t.string :file

      t.timestamps
    end
  end
end
