class CreateVoteScores < ActiveRecord::Migration[5.2]
  def change
    create_table :vote_scores do |t|
      t.references :vote, foreign_key: true
      t.string :body

      t.timestamps
    end
  end
end
