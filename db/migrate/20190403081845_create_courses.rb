class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.datetime :date
      t.references :user, foreign_key: true
      t.string :start_time
      t.string :end_time
      t.integer :course_type

      t.timestamps
    end
  end
end
