# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_06_064037) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "banners", force: :cascade do |t|
    t.string "name"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.datetime "date"
    t.bigint "user_id"
    t.string "start_time"
    t.string "end_time"
    t.integer "course_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_courses_on_user_id"
  end

  create_table "device_files", force: :cascade do |t|
    t.bigint "device_id"
    t.string "name"
    t.integer "category"
    t.integer "file_type"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_device_files_on_device_id"
  end

  create_table "devices", force: :cascade do |t|
    t.string "name"
    t.integer "category"
    t.string "series"
    t.integer "country"
    t.string "image1"
    t.string "image2"
    t.string "image3"
    t.string "image4"
    t.text "description"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exams", force: :cascade do |t|
    t.bigint "operator_id"
    t.datetime "date"
    t.string "exam_time"
    t.integer "score"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["operator_id"], name: "index_exams_on_operator_id"
    t.index ["user_id"], name: "index_exams_on_user_id"
  end

  create_table "lookups", force: :cascade do |t|
    t.string "title"
    t.integer "value"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "operators", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.bigint "user_device_id"
    t.integer "operator_type"
    t.datetime "started_at"
    t.string "phone_number"
    t.integer "exam_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_device_id"], name: "index_operators_on_user_device_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "admin_id"
    t.bigint "user_device_id"
    t.string "certificate"
    t.integer "category"
    t.string "subject"
    t.integer "status"
    t.text "body"
    t.integer "replay_to"
    t.integer "priority"
    t.integer "ward"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_tickets_on_admin_id"
    t.index ["user_device_id"], name: "index_tickets_on_user_device_id"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "user_devices", force: :cascade do |t|
    t.bigint "device_id"
    t.bigint "user_id"
    t.datetime "purchase_date"
    t.datetime "guarantee_started_at"
    t.integer "guarantee_duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_user_devices_on_device_id"
    t.index ["user_id"], name: "index_user_devices_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "company_name"
    t.string "username"
    t.string "password"
    t.string "phone_number"
    t.string "logo"
    t.string "signature"
    t.integer "status"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "verificated_at"
    t.string "verification_code"
  end

  create_table "vote_scores", force: :cascade do |t|
    t.bigint "vote_id"
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["vote_id"], name: "index_vote_scores_on_vote_id"
  end

  create_table "vote_values", force: :cascade do |t|
    t.bigint "vote_id"
    t.bigint "user_id"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_vote_values_on_user_id"
    t.index ["vote_id"], name: "index_vote_values_on_vote_id"
  end

  create_table "votes", force: :cascade do |t|
    t.integer "category"
    t.string "question"
    t.integer "score_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "courses", "users"
  add_foreign_key "device_files", "devices"
  add_foreign_key "exams", "operators"
  add_foreign_key "exams", "users"
  add_foreign_key "operators", "user_devices"
  add_foreign_key "tickets", "admins"
  add_foreign_key "tickets", "user_devices"
  add_foreign_key "tickets", "users"
  add_foreign_key "user_devices", "devices"
  add_foreign_key "user_devices", "users"
  add_foreign_key "vote_scores", "votes"
  add_foreign_key "vote_values", "users"
  add_foreign_key "vote_values", "votes"
end
